import java.util.Scanner;

/**
 * Класс для умножения двух переменных без использования знака умножения
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class Increase {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первый множитель: ");
        int multiplierOne = scanner.nextInt();
        System.out.println("Введите второй множитель: ");
        int multiplierTwo = scanner.nextInt();
        System.out.println("Произведение = " + multiply(multiplierOne, multiplierTwo));
    }

    /**
     * Метод возвращает меньшее из двух значений
     *
     * @param multiplierOne первое значение
     * @param multiplierTwo второе значение
     * @return меньшее число
     */
    private static int min(int multiplierOne, int multiplierTwo) {
        return (multiplierOne <= multiplierTwo) ? multiplierOne : multiplierTwo;
    }

    /**
     * Метод возвращает большее из двух значений
     *
     * @param multiplierOne первое значение
     * @param multiplierTwo второе значение
     * @return большее число
     */
    private static int max(int multiplierOne, int multiplierTwo) {
        return (multiplierOne >= multiplierTwo) ? multiplierOne : multiplierTwo;
    }

    /**
     * Метод умножения двух переменных
     * (без использования знака умножения)
     *
     * @param multiplierOne первый множитель
     * @param multiplierTwo второй множитель
     * @return произведение данных множителей
     */
    private static int multiply(int multiplierOne, int multiplierTwo) {
        int sum = 0;
        if (multiplierOne == 0 || multiplierTwo == 0) {
            return sum;
        }
        if (multiplierOne < 0 && multiplierTwo < 0) {
            multiplierOne = -multiplierOne;
            multiplierTwo = -multiplierTwo;
        }
        int meter = max(multiplierOne, multiplierTwo);
        int summation = min(multiplierOne, multiplierTwo);

        for (int i = 0; i < meter; i++) {
            sum += summation;
        }
        return sum;
    }
}


