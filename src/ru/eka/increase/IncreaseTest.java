package ru.eka.increase;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit тесты для проверки методов класса Increase
 *
 * @author Куцкая Э.А., 15ИТ18
 */
class IncreaseTest {
    @org.junit.jupiter.api.Test
    void min() {
        for (int i = 0; i < 1000; i++) {
            Random random = new Random();
            int one = random.nextInt(400) - 100;
            int two = random.nextInt(400) - 100;
            assertEquals(Math.min(one, two), Increase.min(one, two));
        }
    }

    @org.junit.jupiter.api.Test
    void max() {
        for (int i = 0; i < 1000; i++) {
            Random random = new Random();
            int one = random.nextInt(400) - 100;
            int two = random.nextInt(400) - 100;
            assertEquals(Math.max(one, two), Increase.max(one, two));
        }
    }

    @org.junit.jupiter.api.Test
    void multiply() {
        for (int i = 0; i < 1000; i++) {
            Random random = new Random();
            int one = random.nextInt(400) - 100;
            int two = random.nextInt(400) - 100;
            assertEquals(one*two, Increase.multiply(one,two));
        }
    }
}